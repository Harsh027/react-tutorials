import React, { useState } from "react";

function Counter(props) {
  const [chandu, setChandu] = useState(0);

  const handleIncrement = (e) => {
    setChandu(chandu + 1);
  };

  const handleReset = (e) => {
    setChandu(0);
  };

  return (
    <div>
      <span>{chandu}</span>
      <br />
      <button onClick={(e) => handleIncrement(e)}>Click me</button>
      <button onClick={(e) => handleReset(e)}>Reset me</button>
    </div>
  );
}

export default Counter;
