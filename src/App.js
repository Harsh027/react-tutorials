import React, { useState, useEffect } from "react";
// import logo from "./logo.svg";
import "./App.css";
import Counter from "./components/Counter";

function App() {
  const [name, setName] = useState("");

  useEffect(() => {
    setName(prompt("Enter your name"));
  }, []);

  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
